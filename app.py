import os
import socket
from pymongo import MongoClient
from flask import Flask, url_for, send_from_directory

app = Flask(__name__, static_url_path='', static_folder='assets')

client = MongoClient('mongo', 27017,  username=os.environ['MONGO_INITDB_ROOT_USERNAME'],password=os.environ['MONGO_INITDB_ROOT_PASSWORD'], authSource='admin')
db = client['local']
col = db[ "custom_db" ]


@app.route("/")
def hello():
    item = col.find_one({'date': '29/10/2020'})
    html = "<h3>Hello darkness my old friend</h3>" \
           "<img src={src}>" \
           "<b>Hostname:</b> {hostname}<br/> Кликов {click}"
    if (item is None):
        return html.format(name=os.getenv("NAME", "world"), hostname=socket.gethostname(), click=0, src=url_for('static', filename='img/img.jpg'))
    else:
        return html.format(name=os.getenv("NAME", "world"), hostname=socket.gethostname(), click=item.get('click'), src=url_for('static', filename='img/img.jpg'))

@app.route("/plus")
def plus():
    click = col.find_one({'date': '29/10/2020'})
    if (click is None):
        col.update({
            'date': '29/10/2020',
            'click': 1
        })
    else:
        col.update({
            '_id': click['_id']
        },
         {
            "$set": {'click': int(click.get('click')) + 1}
        })


    html = f"<h3>Клик добавлен</h3><br><a href={url_for('hello')}>Вернуться</a>"
    return html

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)